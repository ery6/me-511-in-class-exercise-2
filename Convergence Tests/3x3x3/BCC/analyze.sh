#!/bin/bash -l
set -e # Stop on error
echo "# lattice constants in Angstrom, energies in eV" > energies.dat
for A in 2.8 2.9 3.0 3.1 3.2 3.3 3.4; do
    # Check for convergence of calculation .
    if (! grep --quiet "Self-consistency cycle converged." <$A/aims.out ) || (! grep --quiet "Have a nice day." <$A/aims.out ); then
        echo " 'pwd'/$A/aims.out did not converge !"
    fi

    # Get 6th column from the line with ” Total energy of the DFT ”.
    E=$(gawk '/\| Total energy of the DFT/ {print $12}' $A/aims.out)

    # Write results to data file .
    echo "$A $E" >> energies.dat
done
