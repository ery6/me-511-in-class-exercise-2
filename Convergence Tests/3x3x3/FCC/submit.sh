#!/bin/bash
#SBATCH --job-name=ME511
#SBATCH --nodes 1
#SBATCH --ntasks-per-node=20
#SBATCH --cpus-per-task=2
#SBATCH -q long
#SBATCH -t 5:00:00

# The following lines set up all environment variables consistently.
# You need these.
module load FHIaims
ulimit -s unlimited

cd $SLURM_SUBMIT_DIR/

# This script assumes that there is already a complete control.in file
# in the directory from which you started. You need to create that control.in file
# before running the script.
#
# The script will then create a series of subdirectories and run FHI-aims for different
# lattice parameters as given in the loop below.
#
# The example is for FCC Si. For BCC Si and for diamond Si, you will have to change
# BOTH the part for the geometry file and the numbers over which the look runs.
#
# Note that the diamond structure has TWO atoms per unit cell, not just one (like the FCC structure)
# and so you will also have to add that atom when you copy / adapt the script to handle the diamond structure.

set -e # Stop on error
for A in 3.5 3.6 3.7 3.8 3.9 4.0 4.1; do
    echo "Processing lattice constant $A Angstrom."
    mkdir $A

    # Use this construct for simple calculations . As values
    # are replaced verbatim , always put them into ”(” , ”)”.
    A2=$(python -c "print ($A/2)")

    # Write geometry . in
    cat >$A/geometry.in <<EOF
    # fcc structure with lattice constant $A Angstrom.
    lattice_vector 0.0 $A2 $A2
    lattice_vector $A2 0.0 $A2
    lattice_vector $A2 $A2 0.0
    atom_frac 0.0 0.0 0.0 Si
EOF

    # Write control . in
    cp control.in $A/control.in

    # Now run FHI−aims in directory $A
    cd $A
    srun -n $SLURM_NTASKS aims.231208.scalapack.mpi.x > aims.out
    cd ..
done
