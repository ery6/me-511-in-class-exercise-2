#!/bin/bash -l
set -e # Stop on error
echo "# lattice constants in Angstrom, energies in eV" > energies.dat
for A in 3.5 3.6 3.7 3.8 3.9 4.0 4.1; do
    # Check for convergence of calculation .
    if (! grep --quiet "Self-consistency cycle converged." <$A/aims.out ) || (! grep --quiet "Have a nice day." <$A/aims.out ); then
        echo " 'pwd'/$A/aims.out did not converge !"
    fi

    # Get 6th column from the line with ” Total energy of the DFT ”.
    E=$(gawk '/\| Total energy of the DFT/ {print $12}' $A/aims.out)\

    # Write results to data file .
    echo "$A $E" >> energies.dat
done
