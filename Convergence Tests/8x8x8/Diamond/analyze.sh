#!/bin/bash -l
set -e # Stop on error
echo "# lattice constants in Angstrom, energies in eV" > energies.dat
for A in 5.1 5.2 5.3 5.4 5.5 5.6 5.7; do
    # Check for convergence of calculation .
    if (! grep --quiet "Self-consistency cycle converged." <$A/aims.out ) || (! grep --quiet "Have a nice day." <$A/aims.out ); then
        echo " 'pwd'/$A/aims.out did not converge !"
    fi

    # Get 6th column from the line with ” Total energy of the DFT ”.
    E=$(gawk '/\| Total energy of the DFT/ {print $12}' $A/aims.out)\
    Eatom=$(python -c "print ($E/2.0)")

    # Write results to data file .
    echo "$A $Eatom" >> energies.dat
done
