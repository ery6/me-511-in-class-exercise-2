# !/bin/bash -l

# This script assumes that there is already a complete control.in file
# in the directory from which you started. You need to create that control.in file
# before running the script.

# The script will then create a series of subdirectories and run FHI-aims for different
# lattice parameters as given in the loop below.

# Note that the diamond structure has TWO atoms per unit cell, not just one (like the FCC structure) 
# and so you will also have to add that atom when you copy / adapt the script to handle the diamond structure.

set -e # Stop on error
module load FHIaims # load the FHIaims module
ulimit -s unlimited
for A in 2.8 2.9 3.0 3.1 3.2 3.3 3.4; do
    echo "Processing lattice constant $A Angstrom."
    mkdir $A

    # Use this construct for simple calculations . As values
    # are replaced verbatim , always put them into ”(” , ”)”.
    A2=$(python -c "print ($A/2)")

    # Write geometry . in
    cat >$A/geometry.in <<EOF
    # bcc structure with lattice constant $A Angstrom.
    lattice_vector $A2 $A2 -$A2
    lattice_vector $A2 -$A2 $A2
    lattice_vector -$A2 $A2 $A2
    atom_frac 0.0 0.0 0.0 Si
EOF

    # Write control . in
    cp control.in $A/control.in

    # Now run FHI−aims with 4 processors in directory $A
    cd $A
    mpirun -np 4 aims.231208.scalapack.mpi.x > aims.out
    cd ..
done
